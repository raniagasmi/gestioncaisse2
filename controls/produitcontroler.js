const produitModel=require('../models/produitmodel')
const multer = require('multer');
const upload = multer({dest: __dirname + '/uploads/images'});
var fs = require("fs")
module.exports={
  ajout : function (req,res) {
    var file = __dirname + '/uploads/' + req.file.originalname;
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(file, data, function (err) {
        if (err) {
          //equiva
          console.error(err);
          var response = {
            message: 'Sorry, file couldn\'t be uploaded.',
            filename: req.file.originalname
          };
        } else {
          const prod = new produitModel({
              nom: req.body.nom,
              description: req.body.description,
              prix: req.body.prix,
              image: req.file.originalname
            }
          )
          prod.save(function (err) {
            if (err) {
              res.json({state: 'no', msg: 'vous avez un erreur ' + err})
            }
            else {
              res.json({state: 'ok', msg: 'produit aded'})
            }

          })
        }
      })
    })
  },

// update
  modif: function (req, res) {
    produitModel.updateOne({_id: req.params.id},{$set:req.body},

      {nom: req.body.nom,
        description:req.body.description,
        prix:req.body.prix,
        image:req.body.image}

      , function (err) {

        if (err) {

          res.json({state: 'no', msg: 'product not found' + err})
        }
        else {
          res.json({state: 'okk', msg: 'product updated' })
        }

      })
  },

  // supprimer
  sup:function (req,res) {
    produitModel.deleteOne({_id:req.params.id},function (err) {
        if(err)
        {
          res.json({state: 'no', msg: 'product not found' + err})
        }
        else {
          res.json({state: 'okk', msg: 'product removed' })

        }

      }
    )
  },

  //getById
 getById:function (req,res) {
    produitModel.findOne({_id: req.params.id},function (err,Liste) {
      if (err) {
        res.json({state: 'no', msg: 'usernot found' + err})
      }
      else {
        res.json(Liste)
      }
      }
    )

 },

  // getAll
  afficher: function (req, res) {

    produitModel.find({}, function (err, Liste) {
      if (err) {

        res.json({state: 'no', msg: 'product found' + err})

      }
      else {

        res.json(Liste)
      }

    })

  },

  getphoto: function (req, res) {



    res.sendFile(__dirname + '/uploads/'+req.params.image)



  },


}
