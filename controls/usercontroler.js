const userModel= require('../models/usermodel');
// module.export hatha yethatou fih les fonction l kol ay whaed lbara maya9rahech
const multer = require('multer');
const upload = multer({dest: __dirname + '/uploads/images'});
var fs = require("fs")
const bcrypt = require('bcrypt')
// const jwt = require('express-jwt'); //module qui assure la securité (json web token)
const jwt = require('jsonwebtoken'); // you should instal jsonwebtoken before (npm i jsonwebtoken)


module.exports= {
//fonctionne
  create: function (req, res) {

    var file = __dirname + '/uploads/' + req.file.originalname;
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(file, data, function (err) {
          if (err) {
            console.error(err);
            var response = {
              message: 'Sorry, file couldn\'t be uploaded.',
              filename: req.file.originalname
            }
          }
          else {

            const user = new userModel({
                nom: req.body.nom,
                prenom: req.body.prenom,
                email: req.body.email,
                password: req.body.password,
                image: req.file.originalname
              }
            )

            user.save(function (err) {
              if (err) {
                res.json({state: 'no', msg: 'vous avez un erreur ' + err})
              }
              else {
                res.json({state: 'ok', msg: 'user aded'})
              }

            })
          }
        }
      )
    })
  },


//fonctionne
  afficher: function (req, res) {

    userModel.find({}, function (err, Liste) {
      if (err) {

        res.json({state: 'no', msg: 'usernot found' + err})

      }
      else {

        res.json(Liste)
      }

    })

  },

//fonctionne
  afficheid: function (req, res) {
    userModel.findOne({_id: req.params.id}).populate({
      path: 'personne',
      populate: {path: 'personnel'}
    }).exec(function (err, user) {
        if (err) {

          res.json({state: 'no', msg: 'user not found' + err})
        }
        else {
          res.json(user)
        }
      }
    )
  },

//push fonctionne
  push: function (req, res) {
    userModel.updateOne({_id: req.params.id}, {$push: {personne: req.body.personne}}, function (err) {

      if (err) {

        res.json({state: 'no', msg: 'user not found' + err})
      }
      else {
        res.json({state: 'okk', msg: 'user updated'})
      }

    })
  },

//modif fonctionne
  modif: function (req, res) {
    userModel.updateOne({_id: req.params.id},{$set:req.body},
      {
        nom: req.body.nom,
        prenom: req.body.prenom,
        email: req.body.email,
        password: req.body.password,
        // image: req.body.image
      }
      // , this.password = bcrypt.hashSync(this.password , 10)


      , function (err) {

        if (err) {

          res.json({state: 'no', msg: 'user not found' + err})
        }
        else {
          res.json({state: 'okk', msg: 'user updated'})
        }

      })
  },


// sup fonctionne
  sup: function (req, res) {
    userModel.deleteOne({_id: req.params.id}, function (err) {
        if (err) {
          res.json({state: 'no', msg: 'user not found' + err})
        }
        else {
          res.json({state: 'okk', msg: 'user removed'})

        }

      })
  },


  upload: function (req, res) {

    var file = __dirname + '/uploads/' + req.file.originalname;
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(file, data, function (err) {
        if (err) {
          console.error(err);
          var response = {
            message: 'Sorry, file couldn\'t be uploaded.',
            filename: req.file.originalname
          }
        }
        else {
          res.json({message: 'photo uploaded'})
        }

      })
    })
  },


  getphoto: function (req, res) {



    res.sendFile(__dirname + '/uploads/'+req.params.image)



  },

  auth : function (req,res) {
    userModel.findOne({email:req.body.email},function (err,userinfo) {
        if (err) {
          next(err);
        }
        else {
          if (userinfo != null) {
            if (bcrypt.compare(req.body.password, userinfo.password)) {
              const token = jwt.sign({id: userinfo.id}, req.app.get('secretKey'), {expiresIn: '1h'})
              res.json({status: "sucess", msg: "user found", data: {user: userinfo, token: token}})

            }

            else {
              res.json({status: "no", message : "password incorrect", data: null})

            }
          }
        else
          {
            res.json({status: "no", message: "email incorrect", data: null})
          }
        }
      }





      )
  },



  getById:function (req,res) {
    userModel.findOne({_id: req.params.id},function (err,Liste) {
        if (err) {
          res.json({state: 'no', msg: 'usernot found' + err})
        }
        else {
          res.json(Liste)
        }
      }
    )

  },









};
