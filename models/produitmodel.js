var mongoose= require('mongoose')
const produitSchema= mongoose.model('produit',new mongoose.Schema(
  {

    nom :{
      type : String,
      required : true,
      trim :true,
    },
    description :{
      type : String,
      // required : false,
      trim :true,
    },
    prix :{
      type : Number,
      required : true,
      trim :true,
    },
    image :{type  : String, required : true,
    },
  })
)
module.exports= produitSchema


