var mongoose= require('mongoose')
 var bcrypt = require ('bcrypt')
const Schema = require('mongoose').Schema;


//classe pere mte3i hekke naml el heritage
const baseoption={

  discriminatorKey :'itemtype', //ynjm ikoun ay hja
  collection :'users', //the name of our collections

}
const userSchema= mongoose.model('User',new mongoose.Schema({
  nom: {
    type: String,
    required : true,
    trim :true,
  },
  prenom: {
    type: String,
    required : true,
    trim :true,
  },
  email: {
    type: String,
    required : true,
    trim :true,
  },
  password: {
    type: String,
    required : true,
    trim :true,
  },
  image: {
    type: String,
    required : false,
  },
}

 , baseoption
)

    .pre("save",function (next) {
      this.password=bcrypt.hashSync(this.password,10);
      next();

    })
);

module.exports= userSchema;
