const clientController=require('../controls/clientcontroller')
const rooter= require('express').Router();

rooter.post('/ajout',clientController.ajout)
rooter.put('/modif/:id',clientController.modif)
rooter.delete('/sup/:id',clientController.sup)
rooter.get('/afficher',clientController.afficher)

 module.exports = rooter ;


