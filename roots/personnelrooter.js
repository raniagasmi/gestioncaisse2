const personelController=require('../controls/personelcontroller')


const router= require('express').Router();
router.get('/afficher',personelController.afficher)
router.post('/ajout',personelController.ajout);
router.put('/modif/:id',personelController.modif);
router.delete('/sup/:id',personelController.sup);

module.exports = router;
