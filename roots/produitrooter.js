const produitcontroler=require('../controls/produitcontroler')
const router=require('express').Router()
const multer = require('multer');
const upload = multer({dest: __dirname + '/uploads/images'});

  router.post('/ajout',upload.single('image'),produitcontroler.ajout);
  router.get('/afficher',produitcontroler.afficher);
  router.get('/getById/:id',produitcontroler.getById);
  router.delete('/sup/:id',produitcontroler.sup);
  router.get('/getphoto/:image',produitcontroler.getphoto)
router.put('/modif/:id',produitcontroler.modif)



module.exports = router;
