const usercontroler=require('../controls/usercontroler')
const multer = require('multer');
const upload = multer({dest: __dirname + '/uploads/images'});


const rooter= require('express').Router();
rooter.post('/create',upload.single('image'),usercontroler.create)
rooter.get('/afficheid/:id', usercontroler.afficheid)
rooter.get('/afficher', usercontroler.afficher)
rooter.put('/push/:id',usercontroler.push)
rooter.put('/modif/:id',usercontroler.modif)
rooter.delete('/sup/:id',usercontroler.sup)
rooter.post('/upload',upload.single('image'),usercontroler.upload)
rooter.get('/getphoto/:image',usercontroler.getphoto)
rooter.post('/auth',usercontroler.auth)
rooter.get('/getById/:id',usercontroler.getById)



module.exports=rooter;
