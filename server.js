const express = require ('express')
// on a importer le package express celui qui va definir les chemin
const bodyParser = require('body-parser') //bodyParser c'est un passerel ya9ra el requete mel body 
var morgan = require ('morgan')
const cors = require('cors')



// router privé
const db=require('./models/db')
const user=require('./roots/userrooter')
const personnel=require('./roots/personnelrooter')
const categorie =require('./roots/categorierooter')
const souscategorie=require('./roots/souscategorierooter')
const produit=require('./roots/produitrooter')
const client=require('./roots/clientrooter')
// express nou permet de diffinir les routes de notre application 
const app =express();
app.use(bodyParser.urlencoded({ extended: false }))// y5alini nparsi el requete mte3i 
app.use(bodyParser.json())

app.set( 'secretKey', 'test')

app.use(morgan('dev'));
app.use(cors('*')) //il permet la partage de ressource entre back et front
app.use('/user',user)
app.use('/personnel',personnel)
app.use('/categorie',categorie)
app.use('/souscategorie',souscategorie)
app.use('/produit',produit)
app.use('/client',client)

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// handle errors
app.use(function (err, req, res, next) {
  console.log(err);

  if (err.status === 404)
    res.status(404).json({message: "Not found"});
  else
    res.status(500).json({message: "Something looks wrong :( !!!"});
});

app.listen(3000, function () {

  console.log('bienveune')

})
