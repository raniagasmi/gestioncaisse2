import { NgModule } from '@angular/core';
import {Routes, RouterModule, CanActivate} from '@angular/router';
import {HomeComponent} from './home/home.component';

import {DetailComponent} from "./home/detail/detail.component";
import {LoginComponent} from "./login/login.component";
import {AuthgradService} from "./service/authgrad.service";
import {CalendarComponent} from "./home/calendar/calendar.component";


const routes: Routes = [
  {path:'',component : LoginComponent},
  {path : 'home', component : HomeComponent,canActivate:[AuthgradService], children : [

      {path : 'gestionProduit',
      loadChildren : './home/gestion-produit/gestion-produit.module#GestionProduitModule'
      },

      {path : 'client',
        loadChildren : './home/client/client.module#ClientModule'
      },

      {path : 'personnel',
        loadChildren : './home/personnel/personnel.module#PersonnelModule'
      },
      {path : 'calandrier',component:CalendarComponent},
      {path:'detail/:id',component:DetailComponent}
      ] },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
