import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import Swal from 'sweetalert2';
import {UserService} from "../../../service/user.service";
@Component({
  selector: 'app-ajout-client',
  templateUrl: './ajout-client.component.html',
  styleUrls: ['./ajout-client.component.css']
})
export class AjoutClientComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private userService : UserService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });

}
  get f() { return this.registerForm.controls; }

  ajouterClient() {
    this.submitted = true;//

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.userService.ajouterCLient(this.registerForm.value).subscribe(res => {
       console.log(res);
      Swal.fire(
        'Good job!',
        'You clicked the button!',
        'success'
      )
      this.submitted = false ;
this.registerForm.reset()

    })
  }
}
