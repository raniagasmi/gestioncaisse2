import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListeClientComponent} from './liste-client/liste-client.component';
import {AjoutClientComponent} from "./ajout-client/ajout-client.component";


const routes: Routes = [

  {path : 'listeClient', component : ListeClientComponent},
  {path : 'ajoutClient', component : AjoutClientComponent}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
