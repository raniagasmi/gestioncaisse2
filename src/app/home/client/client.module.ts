import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { ListeClientComponent } from './liste-client/liste-client.component';
import { AjoutClientComponent } from './ajout-client/ajout-client.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [ListeClientComponent, AjoutClientComponent],
  imports: [
    CommonModule,
    ClientRoutingModule,
FormsModule,
    ReactiveFormsModule,




  ]
})
export class ClientModule { }
