import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../service/user.service';
import Swal from "sweetalert2";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-liste-client',
  templateUrl: './liste-client.component.html',
  styleUrls: ['./liste-client.component.css']
})
export class ListeClientComponent implements OnInit {
  ListeUser ;
  EditForm:FormGroup;
  submitted = false;
_id;
  constructor(private userService: UserService, private formBuilder:FormBuilder) {
    this.afficherUser();

  }

  ngOnInit() {
    this.EditForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  })
  }
  recupere(_id,nom,prenom,email,password){
    this._id=_id;
    this.EditForm.get('nom').setValue(nom);
    this.EditForm.get('prenom').setValue(prenom);
    this.EditForm.get('email').setValue(email);
    this.EditForm.get('password').setValue(password);
console.log(nom)
  }
  get f() { return this.EditForm.controls; }

afficherUser (){

    this.userService.getAllUser().subscribe(res=>{

      console.log(res)

      this.ListeUser = res ;


    })

  }

  DeleteUser(id) {
    Swal.fire({
          title: 'vous êtes sûr?',
          text: "Vous ne pourrez pas revenir en arriére !",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#d6100f',
          cancelButtonColor: '#3dc405',
          confirmButtonText: 'Oui, Supprimer!',
          cancelButtonText:'Annule'
        })
          .then((result) => {
            if (result.value) {
          return this.userService.deleteUser(id)
            .subscribe(res => {
              console.log(res);
              this.afficherUser();
            })
        }
      })
  }



update(){
  this.submitted = true

    this.userService.modifierClient(this._id,this.EditForm.value).subscribe(res=>{

      console.log(res);
    })
  this.afficherUser();
}



}
