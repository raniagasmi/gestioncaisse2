import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
id;
user;
  constructor(private actroute:ActivatedRoute , private service:UserService) {
    this.id=this.actroute.params['value']['id'];
    this.getById(this.id);
  }

  ngOnInit() {
  }
getById(id){
    console.log(this.id)
  this.service.getById(id).subscribe(res=>{
    this.user=res;

    // console.log(this.user.nom)
    console.log(res)
  })
}


}
