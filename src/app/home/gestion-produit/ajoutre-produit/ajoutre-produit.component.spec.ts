import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutreProduitComponent } from './ajoutre-produit.component';

describe('AjoutreProduitComponent', () => {
  let component: AjoutreProduitComponent;
  let fixture: ComponentFixture<AjoutreProduitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutreProduitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutreProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
