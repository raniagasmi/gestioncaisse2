import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProduitService} from "../../../service/produit.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-ajoutre-produit',
  templateUrl: './ajoutre-produit.component.html',
  styleUrls: ['./ajoutre-produit.component.css']
})
export class AjoutreProduitComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private produitService: ProduitService) {
  }

  fileToApload: Array<File>
  image;

  ngOnInit() {
    // amalna formulaire esmha this.registerForm
    this.registerForm = this.formBuilder.group({
      nom: ['', Validators.required],
      description: ['', Validators.required],
      prix: ['', [Validators.required]],
//image:['']

    });
  }

  get f() {
    return this.registerForm.controls;
  }

  ajouterProduit() {
    this.submitted = true;
    console.log(this.fileToApload[0])
    console.log(this.registerForm.value)
    this.produitService.ajouterProduit(this.registerForm.value, this.fileToApload[0]).subscribe(res => {
        console.log(res);
        Swal.fire(
          'Good job!',
          'You clicked the button!',
          'success')
        this.submitted = false;
        this.registerForm.reset()
      }
    )
  }


  //hathi juste bech tatini esm  l file  size
  recupereFile(file) {
    this.fileToApload = file.target.files as Array<File>
    this.image = file.target.files[0].name;
  }

}
