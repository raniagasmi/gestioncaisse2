import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AjoutreProduitComponent} from './ajoutre-produit/ajoutre-produit.component';
import {ListeProduitComponent} from './liste-produit/liste-produit.component';


const routes: Routes = [


  {path : 'ajouterProduit', component : AjoutreProduitComponent},
  {path : 'listeProduit', component : ListeProduitComponent}



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionProduitRoutingModule { }
