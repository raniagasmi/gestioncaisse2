import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GestionProduitRoutingModule } from './gestion-produit-routing.module';
import { AjoutreProduitComponent } from './ajoutre-produit/ajoutre-produit.component';
import { ListeProduitComponent } from './liste-produit/liste-produit.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [AjoutreProduitComponent, ListeProduitComponent],
  imports: [
    CommonModule,
    GestionProduitRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class GestionProduitModule { }
