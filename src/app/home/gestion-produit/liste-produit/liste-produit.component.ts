import { Component, OnInit } from '@angular/core';
import {ProduitService} from "../../../service/produit.service";
import Swal from "sweetalert2";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-liste-produit',
  templateUrl: './liste-produit.component.html',
  styleUrls: ['./liste-produit.component.css']
})
export class ListeProduitComponent implements OnInit {
  listeProduit;
  _id;
  EditForm:FormGroup;
  submitted = false;

  constructor(private produitService :ProduitService,private formBuilder:FormBuilder)
  {  this.afficherproduit();}

  ngOnInit() {
    this.EditForm = this.formBuilder.group({
      nom: ['', Validators.required],
      description: ['', Validators.required],
      prix: ['', [Validators.required, Validators.email]],
      image: ['', [Validators.required, Validators.minLength(6)]]
    })
  }



  afficherproduit (){

    this.produitService.getAllProduct().subscribe(res=>{
      console.log(res)
      this.listeProduit = res ;

    })
  }



  DeleteProduct(id) {
    Swal.fire({
      title: 'vous êtes sûr?',
      text: "Vous ne pourrez pas revenir en arriére !",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d6100f',
      cancelButtonColor: '#3dc405',
      confirmButtonText: 'Oui, Supprimer!',
      cancelButtonText:'Annule'
    })
      .then((result) => {
        if (result.value) {
          return this.produitService.deleteProduct(id)
            .subscribe(res => {
              console.log(res);
              this.afficherproduit();
            })
        }
      })
  }

// on recupere les donnée pour les afficher dans le editForm en clicant sue edit HTML
  recupere(_id,nom,description,prix,image){
    this._id=_id;
    this.EditForm.get('nom').setValue(nom);
    this.EditForm.get('description').setValue(description);
    this.EditForm.get('prix').setValue(prix);
    this.EditForm.get('image').setValue(image);
    console.log(nom)
  }


  //controle de champ
  get f() { return this.EditForm.controls; }s



  update(){
    this.submitted = true

    this.produitService.updateProduct(this._id,this.EditForm.value).subscribe(res=>{

      console.log(res);
    })
    this.afficherproduit();
  }


}
