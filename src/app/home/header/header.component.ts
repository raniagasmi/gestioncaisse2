import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  info;
  username;

  constructor() { }

  ngOnInit() {
    this.info=JSON.parse(localStorage.getItem('userConnecte'))
  }

  deconnecter(){
    localStorage.clear();
  }



}
