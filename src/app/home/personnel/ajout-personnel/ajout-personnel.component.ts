import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PersonnelService} from "../../../service/personnel.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-ajout-personnel',
  templateUrl: './ajout-personnel.component.html',
  styleUrls: ['./ajout-personnel.component.css']
})
export class AjoutPersonnelComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private personnelService:PersonnelService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  get f() { return this.registerForm.controls; }

  ajouterPersonnel() {
    this.submitted = true;//

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.personnelService.ajouterPersonnel(this.registerForm.value).subscribe(res => {
      console.log(res);
      Swal.fire(
        'Good job!',
        'You clicked the button!',
        'success'
      )
      this.submitted = false ;
      this.registerForm.reset()

    })
  }



}
