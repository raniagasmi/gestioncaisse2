import { Component, OnInit } from '@angular/core';
import {PersonnelService} from "../../../service/personnel.service";
import Swal from "sweetalert2";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-liste-personnel',
  templateUrl: './liste-personnel.component.html',
  styleUrls: ['./liste-personnel.component.css']
})
export class ListePersonnelComponent implements OnInit {
  ListePersonnel;
  EditForm:FormGroup;
  submitted = false;

  _id;
  constructor(private personnelService:PersonnelService,private formBuilder :FormBuilder ) {
    this.afficherPersonnel();
  }

  ngOnInit() {
    this.EditForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }
  recupere(_id,nom,prenom,email,password){
    this._id=_id;
    this.EditForm.get('nom').setValue(nom);
    this.EditForm.get('prenom').setValue(prenom);
    this.EditForm.get('email').setValue(email);
    this.EditForm.get('password').setValue(password);
    console.log(nom)
  }
  get f() { return this.EditForm.controls; }

  update(){
    this.submitted = true

    this.personnelService.modifierPersonnel(this._id,this.EditForm.value).subscribe(res=>{

      console.log(res);
    })
    this.afficherPersonnel();
  }


  afficherPersonnel(){


      this.personnelService.afficherPersonnel().subscribe(res=>{

        console.log(res)

        this.ListePersonnel = res ;


      })



  }

  DeletePersonnel(id) {
    Swal.fire({
      title: 'vous êtes sûr?',
      text: "Vous ne pourrez pas revenir en arriére !",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d6100f',
      cancelButtonColor: '#3dc405',
      confirmButtonText: 'Oui, Supprimer!',
      cancelButtonText:'Annule'
    })
      .then((result) => {
        if (result.value) {
          return this.personnelService.deletePersonnel(id)
            .subscribe(res => {
              console.log(res);
              this.afficherPersonnel();
            })
        }
      })
  }


}
