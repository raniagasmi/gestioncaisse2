import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListePersonnelComponent} from "./liste-personnel/liste-personnel.component";
import {AjoutPersonnelComponent} from "./ajout-personnel/ajout-personnel.component";


const routes: Routes = [
  {path:'listePersonnel',component:ListePersonnelComponent},
  {path:'ajoutPersonnel',component:AjoutPersonnelComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonnelRoutingModule { }
