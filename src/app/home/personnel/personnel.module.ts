import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonnelRoutingModule } from './personnel-routing.module';
import { AjoutPersonnelComponent } from './ajout-personnel/ajout-personnel.component';
import { ListePersonnelComponent } from './liste-personnel/liste-personnel.component';
import {FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [AjoutPersonnelComponent, ListePersonnelComponent],
  imports: [
    CommonModule,
    PersonnelRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PersonnelModule { }

