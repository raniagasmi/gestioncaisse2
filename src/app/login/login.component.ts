import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../service/login.service";
import Swal from "sweetalert2";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  LoginForm:FormGroup;
  submitted = false;

  constructor(private loginService: LoginService, private formBuilder:FormBuilder, private router:Router) { }

  ngOnInit() {
    this.LoginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }
  get f() { return this.LoginForm.controls; }


verification(){
  this.submitted = true;

console.log(this.LoginForm.value)//juste bech nchouf behe l contenu mte3 l form
  this.loginService.onSubmit(this.LoginForm.value).subscribe(res => {
  console.log(res)

    if (JSON.parse(JSON.stringify(res)).status === "sucess") {


      localStorage.setItem('user', JSON.stringify(JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).data)).user));


      localStorage.setItem('token', JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).data.token)));


      console.log('message',JSON.parse(localStorage.getItem('userConnecte')))


      Swal.fire(
        'OPPS',
        'Authentification avec success',
        'success'
      )
this.router.navigate(['home'])
    }

    else {

      Swal.fire(
        'OPPS',
        'Verifier votre Email et password',
        'error'
      )

    }


});
}
}
