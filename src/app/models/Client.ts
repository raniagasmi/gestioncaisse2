export class CLient {

  private _nom : string ;
  private _prenom : string ;
  private _email  : string ;
  private _password : string ;


  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get prenom(): string {
    return this._prenom;
  }

  set prenom(value: string) {
    this._prenom = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  constructor(nom: string, prenom: string, email: string, password: string) {
    this._nom = nom;
    this._prenom = prenom;
    this._email = email;
    this._password = password;
  }
}
