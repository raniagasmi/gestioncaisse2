import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
baseUrl=environment.baseUrl;
  constructor(private http:HttpClient) { }

  onSubmit(user)
  {

    return this.http.post(this.baseUrl+'/user/auth' ,user)


  }

}
