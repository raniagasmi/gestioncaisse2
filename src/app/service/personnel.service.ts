import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PersonnelService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  afficherPersonnel() {
    return this.http.get(this.baseUrl + '/personnel/afficher')
  }
  ajouterPersonnel(personnel){

    return this.http.post(this.baseUrl + '/personnel/ajout', personnel)

  }
  modifierPersonnel(_id,personnel){

    return this.http.put(this.baseUrl + '/personnel/modif/'+_id, personnel)

  }

  getById(id)
  {return this.http.get(this.baseUrl+'/personnel/getById/'+id)}

  deletePersonnel(_id : string){
    return this.http.delete(this.baseUrl + '/personnel/sup/'+_id)

  }



}
