import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Produit} from "../models/produit";

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
  baseUrl = environment.baseUrl

  constructor(private http: HttpClient) {
  }

//bech nadi image lazem namel hekka 9a3din n3abiw fl form data bech na5ou image 5aterha matetada kan bl form data


  produit = new Produit();

  ajouterProduit(produit, fileToUpload) {

    console.log("prod : ",produit)
    console.log("image : ",fileToUpload)

    let formData = new FormData();
    formData.append('nom', '' + produit.nom)
    formData.append('description', '' + produit.description)
    formData.append('prix', '' + produit.prix)
    formData.append('image', fileToUpload);

    return this.http.post(this.baseUrl + '/produit/ajout', formData)
  }


  getAllProduct() {
    return this.http.get(this.baseUrl + '/produit/afficher')
  }

  deleteProduct(_id: string) {
    return this.http.delete(this.baseUrl + '/produit/sup/' + _id)

  }

 updateProduct(_id,produit){

   return this.http.put(this.baseUrl + '/produit/modif/'+_id ,produit)

  }

}
