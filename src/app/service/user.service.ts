import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environment.baseUrl

  constructor(private http : HttpClient) { }

  getAllUser(){
    return this.http.get(this.baseUrl+'/client/afficher' )
  }

  deleteUser(_id : string){
    return this.http.delete(this.baseUrl + '/client/sup/'+_id)

  }


  ajouterCLient(Client){

    return this.http.post(this.baseUrl + '/client/ajout', Client)

  }


  modifierClient(_id,client){

    return this.http.put(this.baseUrl + '/client/modif/'+_id, client)

  }

  getById(id)
  {return this.http.get(this.baseUrl+'/user/getById/'+id)}

}
